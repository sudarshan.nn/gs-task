<?php

namespace Tests\Unit;

use PHPUnit\Framework\TestCase;
use App\Services\Providers\MyCorporateInfoProvider;

class MyCorporateInfoProviderTest extends TestCase
{
    /**
     * Test crawl
     *
     * @return void
     */
    public function testCrawlIndustries()
    {
        $industries = (new MyCorporateInfoProvider)->crawlIndustries();
        echo $industries[0]['url'];
        $this->assertTrue(isset($industries[0]['name']));
        $this->assertTrue(isset($industries[0]['url']));
    }

    /**
     * Test crawl
     *
     * @return void
     */
    public function testCrawlIndustryCompanies()
    {
        $url = 'http://www.mycorporateinfo.com/industry/section/G';
        $companies = (new MyCorporateInfoProvider)->crawlIndustryCompanies($url);
        $this->assertTrue(isset($companies[0]['cin']));
        $this->assertTrue(isset($companies[0]['company_name']));
        $this->assertTrue(isset($companies[0]['class']));
        $this->assertTrue(isset($companies[0]['status']));
        $this->assertTrue(isset($companies[0]['url']));
    }

    /**
     * Test crawl
     *
     * @return void
     */
    public function testGetCompanyDetails()
    {
        $url = 'http://www.mycorporateinfo.com/business/ratan-glitter-industries-limited-1';
        $companyDetails = (new MyCorporateInfoProvider)->getCompanyDetails($url);
        $this->assertTrue(isset($companyDetails['cin']));
    }
}
