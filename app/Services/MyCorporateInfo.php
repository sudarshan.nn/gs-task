<?php

namespace App\Services;
use App\Services\Interfaces\CompanyCrawlerInterface;
use  App\Services\Providers\MyCorporateInfoProvider;

class MyCorporateInfo implements CompanyCrawlerInterface
{    
    /**
     * Get all the industries
     *
     * @return void
     */
    public function getIndustries() : array
    {
        $industries = (new MyCorporateInfoProvider)->crawlIndustries();
        return $industries;
    }
    
    /**
     * Get all companies of a particular industry
     *
     * @param  mixed $industry
     * @return array
     */
    public function getCompanies(string $industry_url) : array
    {
        $companies = (new MyCorporateInfoProvider)->crawlIndustryCompanies($industry_url);
        return $companies;
    }
    
    /**
     * Get all details of a particular company
     *
     * @param  mixed $company
     * @return array
     */
    public function getCompanyDetails(string $company_url) : array
    {
        $companyDetails = (new MyCorporateInfoProvider)->getCompanyDetails($company_url);
        return $companyDetails;
    }
}
