<?php

namespace App\Services\Interfaces;
use App\Minkspay\Models\Payout;

interface CompanyCrawlerInterface
{
    public function getIndustries() : array;

    public function getCompanies(string $industry_url) : array;

    public function getCompanyDetails(string $company_url) : array;
}