<?php 
namespace App\Services\Providers;
use DOMDocument;
error_reporting(0);

class Crawler {

    public static $timeout = 30;
    public static $agent   = 'Mozilla/5.0 (X11; Linux x86_64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/86.0.4240.111 Safari/537.36';

    public static function httpRequest($url) {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,            $url);
        curl_setopt($ch, CURLOPT_USERAGENT,      self::$agent);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, self::$timeout);
        curl_setopt($ch, CURLOPT_TIMEOUT,        self::$timeout);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    public static function strip_whitespace($data) {
        $data = preg_replace('/\s+/', ' ', $data);
        return trim($data);
    }

    public static function getElementsByTag($data, $tag) {
        $response = array();
        $dom      = new DOMDocument;
        @$dom->loadHTML($data); 
        foreach ( $dom->getElementsByTagName($tag) as $index => $element ) {
            $response[$index]['text'] = self::strip_whitespace($element->nodeValue);
            foreach ( $element->attributes as $attribute ) {
                $response[$index]['attributes'][strtolower($attribute->nodeName)] = self::strip_whitespace($attribute->nodeValue);
            }
        }
        return $response;
    }

    public static function getElementsByTagForTableRow($data, $tag) {
        $response = array();
        $dom      = new DOMDocument;
        @$dom->loadHTML($data); 
        foreach ( $dom->getElementsByTagName($tag) as $index => $element ) {
            
            foreach ( $element->childNodes as $child_index => $childNode ) {
                $response[$index]['nodeValue'][$child_index] = self::strip_whitespace($childNode->nodeValue);
                $response[$index]['nodeValue'][$child_index] = self::strip_whitespace($childNode->nodeValue);

                foreach($childNode->childNodes as $sub_child_index => $sub_childNode){
                    if(isset($sub_childNode->tagName) && $sub_childNode->tagName == 'a'){
                        if(isset($sub_childNode->attributes[0])){
                            $response[$index]['link'] = $sub_childNode->attributes[0]->value;
                        }
                    }
                }
            }
        }
        return $response;
    }

    public static function getElementsByClassName($data, $ClassName, $tagName=null) {
        $dom      = new \DOMDocument;
        @$dom->loadHTML($data);
        if($tagName){
            $Elements = $dom->getElementsByTagName($tagName);
        }else {
            $Elements = $dom->getElementsByTagName("*");
        }
        
        $Matched = array();
        for($i=0;$i<$Elements->length;$i++) {
            if($Elements->item($i)->attributes->getNamedItem('class')){
                if($Elements->item($i)->attributes->getNamedItem('class')->nodeValue == $ClassName) {
                    $Matched[]=$Elements->item($i);
                }
            }
        }
        return $Matched;
    }

}?>