<?php 
namespace App\Services\Providers;

class MyCorporateInfoProvider {

    function __construct() {
        $this->base_url = 'http://www.mycorporateinfo.com';
    }

    public function crawlIndustries() 
    {
        $url    = $this->base_url . "/industry";
        $data   = Crawler::httpRequest($url);        
        #$industries    = Crawler::getElementsByClassName($data, 'list-group-item');
        $industries     = Crawler::getElementsByTag($data, 'a');
        $formattedIndustries = [];
        foreach($industries as $industry){
            if (!isset($industry['attributes']['href']))
            {
                continue;
            }
            $formattedIndustry = [
                'name' => trim($industry['text'],  "."),
                'url' => $this->base_url . $industry['attributes']['href'],
            ];

            array_push($formattedIndustries, $formattedIndustry);
        }
        return array_slice($formattedIndustries, 12);
    }
    
    public function crawlIndustryCompanies(string $industry_url)
    {
        $data       = Crawler::httpRequest($industry_url);
        $companies  = Crawler::getElementsByTagForTableRow($data, 'tr');
        $formattedCompanies = [];
        foreach ($companies as $index => $company) {
            if($index == 0){
                continue;
            }
            $formattedCompany = [
                'cin' => $company['nodeValue'][0],
                'company_name' => $company['nodeValue'][1],
                'class' => $company['nodeValue'][2],
                'status' => $company['nodeValue'][3],
                'url' => $this->base_url . $company['link'],
            ];

            array_push($formattedCompanies, $formattedCompany);
        }
        return $formattedCompanies;
    }

    public function getCompanyDetails(string $company_url)
    {
        $data       = Crawler::httpRequest($company_url);
        $companies  = Crawler::getElementsByTagForTableRow($data, 'tr');
        $companyDetails = [];
        $associativeArray = array();

        foreach ($companies as $index => $company) {
            if(count($company['nodeValue']) != 2){
                continue;
            }
            
            $formattedKey = $this->getFormattedKeys($company['nodeValue'][0]);
            $companyDetails[$formattedKey] = $company['nodeValue'][1];

        }

        return $companyDetails;
    }

    public function getFormattedKeys($key)
    {
        if($key == 'Corporate Identification Number')
            return 'cin';

        if($key == 'Company Name')
            return 'company_name';

        if($key == 'Company Status')
            return 'company_status';

        if($key == 'Company Status')
            return 'company_status';

        if($key == 'Age (Date of Incorporation)')
            return 'date_of_incorporation';

        if($key == 'Registration Number')
            return 'registration_no';

        if($key == 'Company Category')
            return 'company_category';

        if($key == 'Company Subcategory')
            return 'company_sub_category';

        if($key == 'Class of Company')
            return 'company_class';

        if($key == 'ROC Code')
            return 'roc_code';

        if($key == 'Number of Members (Applicable only in case of company without Share Capital)')
            return 'member_count';

        if($key == 'Email Address')
            return 'email_address';

        if($key == 'Registered Office')
            return 'registered_office';

        if($key == 'Whether listed or not')
            return 'list_status';

        if($key == 'Date of Last AGM')
            return 'date_of_last_agm';

        if($key == 'Date of Balance sheet')
            return 'date_of_balance_sheet';

        if($key == 'State')
            return 'state';

        if($key == 'District')
            return 'district';

        if($key == 'City')
            return 'city';

        if($key == 'PIN')
            return 'pin';

        if($key == 'Section')
            return 'section';

        if($key == 'Division')
            return 'division';

        if($key == 'Main Group')
            return 'main_group';

        if($key == 'Main Class')
            return 'main_class';
    }

}
?>