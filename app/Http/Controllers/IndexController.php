<?php

namespace App\Http\Controllers;
use App\Services\CompanyFactory;

class IndexController extends Controller
{

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke()
    {
        $industries = (new CompanyFactory)->initialize()->getIndustries();
        $data = [
            'industries' => $industries
        ];
        return view('welcome', $data);
    }

}
