<?php

namespace App\Http\Controllers;
use App\Services\CompanyFactory;
use Illuminate\Http\Request;

class IndustryCompaniesController extends Controller
{

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $companies = (new CompanyFactory)->initialize()->getCompanies($request->url);
        $data = [
            'companies' => $companies
        ];
        return view('companies', $data);
    }

}
