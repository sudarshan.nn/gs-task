<?php

namespace App\Http\Controllers;
use App\Services\CompanyFactory;
use Illuminate\Http\Request;

class CompanyDetailsController extends Controller
{

    /**
     * Handle the incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function __invoke(Request $request)
    {
        $companyDetails = (new CompanyFactory)->initialize()->getCompanyDetails($request->url);
        $data = [
            'company' => $companyDetails
        ];
        return view('company-details', $data);
    }

}
