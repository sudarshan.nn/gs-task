<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">

        <title>Laravel</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css2?family=Nunito:wght@200;600&display=swap" rel="stylesheet">

        <!-- Styles -->
        <style>
            html, body {
                background-color: #fff;
                color: #636b6f;
                font-family: 'Nunito', sans-serif;
                font-weight: 200;
                height: 100vh;
                margin: 0;
            }

            .full-height {
                height: 100vh;
            }

            .flex-center {
                align-items: center;
                display: flex;
                justify-content: center;
            }

            .position-ref {
                position: relative;
            }

            .top-right {
                position: absolute;
                right: 10px;
                top: 18px;
            }

            .content {
                text-align: center;
            }

            .title {
                font-size: 84px;
            }

            .links > a {
                color: #636b6f;
                padding: 0 25px;
                font-size: 13px;
                font-weight: 600;
                letter-spacing: .1rem;
                text-decoration: none;
                text-transform: uppercase;
            }

            .m-b-md {
                margin-bottom: 30px;
            }
        </style>
    </head>
    <body>
        <div class="position-ref full-height">

            <div class="content">
                <div class="title m-b-md">
                    Global Shiksha Task
                </div>
                <div class="title m-b-md" style="font-size: 48px; color: blue">
                    Company Details - {{$company['company_name']}}
                </div>


                <div class="links" >

                    <table class="links table table-striped" style="margin: auto; text-align: left">
                        <tr>
                            <td style="font-weight: bold"> CIN </td>
                            <td> {{$company['cin']}} </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold"> COMPANY NAME </td>
                            <td> {{$company['company_name']}} </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold"> COMPANY STATUS </td>
                            <td> {{$company['company_status']}} </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold"> INCORPORATION DATE </td>
                            <td> {{$company['date_of_incorporation']}} </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold"> REGISTRATION NO. </td>
                            <td> {{$company['registration_no']}} </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold"> COMPANY CATEGORY </td>
                            <td> {{$company['company_category']}} </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold"> COMPANY SUB-CATEGORY </td>
                            <td> {{$company['company_sub_category']}} </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold"> COMPANY CLASS </td>
                            <td> {{$company['company_class']}} </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold"> ROC CODE </td>
                            <td> {{$company['roc_code']}} </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold"> Number of members </td>
                            <td> {{$company['member_count']}} </td>
                        </tr>
                        <tr>
                            <td style="font-weight: bold"> Email Address </td>
                            <td> {{$company['email_address']}} </td>
                        </tr>

                        <tr>
                            <td style="font-weight: bold"> Registered Address </td>
                            <td> {{$company['registered_office']}} </td>
                        </tr>

                        <tr>
                            <td style="font-weight: bold"> Date of last AGM </td>
                            <td> {{$company['date_of_last_agm']}} </td>
                        </tr>

                        <tr>
                            <td style="font-weight: bold"> Date of Balance Sheet </td>
                            <td> {{$company['date_of_balance_sheet']}} </td>
                        </tr>

                        <tr>
                            <td style="font-weight: bold"> State </td>
                            <td> {{$company['state']}} </td>
                        </tr>

                        <tr>
                            <td style="font-weight: bold"> District </td>
                            <td> {{$company['district']}} </td>
                        </tr>

                    </table>

                </div>

                <div class="title m-b-md" style="font-size: 30px; margin-top: 30px;">
                    Submitted By: Sudarshan DK
                </div>
            </div>
        </div>
    </body>
</html>
